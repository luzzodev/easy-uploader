import os
import logging
import shutil

from typing import Optional

from fastapi import APIRouter, UploadFile, File, Form
from bighdfs.constants import BASE_APP_FILES_DIR

router = APIRouter()

logger = logging.getLogger(__name__)


@router.delete('/file/delete/{app_id}')
@router.delete('/file/delete/{app_id}/{file_name}')
def delete_file(app_id: str, file_name: Optional[str] = None):
    if ".." in [app_id, file_name]:
        return {"status": "error"}

    app_path = os.path.join(BASE_APP_FILES_DIR, app_id)
    if file_name:
        file_path = os.path.join(app_path, file_name)
        if os.path.exists(file_path):
            logger.info(f"Deleting : {file_path}")
            os.remove(file_path)
    else:
        if os.path.exists(app_path):
            logger.info(f"Deleting app path at: {app_path}")
            shutil.rmtree(app_path)

    return {"status": "ok"}


@router.post('/file/upload')
async def create_upload_file(application_id: str = Form(...), file: UploadFile = File(...)):
    dst_path = os.path.join(BASE_APP_FILES_DIR, application_id)
    if not os.path.exists(dst_path):
        logger.info(f"Creating path at : {dst_path}")
        os.mkdir(dst_path)
    dst_file_path = os.path.join(dst_path, file.filename)
    with open(dst_file_path, "wb") as uploaded_file:
        while True:
            chunk = await file.read(1024)
            if len(chunk) <= 0:
                break
            uploaded_file.write(chunk)
    logger.info(f"New file uploaded at : {dst_file_path}")
    return {"filename": file.filename, "appID": application_id}

