import logging
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi

from bighdfs.router import api


logger = logging.getLogger(__name__)

tags_metadata = [
    {
        "name": "file",
        "description": "Operations with files.",
    },
]

app = FastAPI()
app.include_router(api.router, tags=['api'], prefix='/api')


origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="BIG HDFS",
        description="Descrizione della documentazione",
        version="1.0.0",
        routes=app.routes,
        tags=tags_metadata,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCEt"
               "LSBHZW5lcmF0b3I6IEdyYXZpdC5pbyAtLT48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbm"
               "s9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHN0eWxlPSJpc29sYXRpb246aXNvbGF0ZSIgdmlld0JveD0iMCAwIDk0LjM3"
               "NSA5MS4xNjIiIHdpZHRoPSI5NC4zNzVwdCIgaGVpZ2h0PSI5MS4xNjJwdCI+PGRlZnM+PGNsaXBQYXRoIGlkPSJfY2xpcFBhdGhfbE"
               "1BbG5mWkdYV2dJek5Jek1TZFdhU2k0cG9NOHozWXkiPjxyZWN0IHdpZHRoPSI5NC4zNzUiIGhlaWdodD0iOTEuMTYyIi8+PC9jbGlw"
               "UGF0aD48L2RlZnM+PGcgY2xpcC1wYXRoPSJ1cmwoI19jbGlwUGF0aF9sTUFsbmZaR1hXZ0l6Tkl6TVNkV2FTaTRwb004ejNZeSkiPj"
               "xnPjxnPjxwYXRoIGQ9IiBNIDQ5LjIwNiA4Ny4wNSBDIDQ4LjIyNCA3Ni41MiA0Ny42NjcgNjUuOTc5IDQ4LjE0OCA1NS40MTIgQyA0"
               "OC4zNTYgNTAuODQzIDQ4LjY5IDQ2LjI3NSA1MC4wMjUgNDEuODYzIEMgNTIuODgyIDMyLjQyNSA1OC44OTIgMjYuMDY1IDY4LjQ4MS"
               "AyMy40MDYgQyA3OC4zMjQgMjAuNjc2IDg4LjQzOSAyNy4zNTcgODkuOTI0IDM3LjQzNiBDIDkxLjI2OCA0Ni41NTcgODUuNTE5IDU0"
               "LjgzNiA3Ni40MzIgNTYuOTE3IEMgNzMuMjQ0IDU3LjY0NyA3MC4wNTMgNTguMzc3IDY2LjkwOSA1OS4yNjYgQyA2NC42NTEgNTkuOT"
               "A1IDYyLjYgNjEuMDY0IDYwLjYzNCA2Mi4zNjQgQyA1Ny45NCA2NC4xNDYgNTUuOTU0IDY2LjQ3OSA1NC42NDEgNjkuNDQzIEMgNTIu"
               "NTQyIDc0LjE4MiA1MS4xMzMgNzkuMTM1IDQ5Ljg5IDg0LjE0NSBDIDQ5LjY1IDg1LjExMSA0OS40MzMgODYuMDgxIDQ5LjIwNiA4Ny"
               "4wNSBaICIgZmlsbC1ydWxlPSJldmVub2RkIiBmaWxsPSJyZ2IoMCw5NywxNjUpIi8+PHBhdGggZD0iIE0gNDQuNzg4IDY2LjU5MiBD"
               "IDQ0LjA4OSA2NS42NDQgNDMuNTA4IDY0Ljc0OSA0Mi44MjEgNjMuOTQ2IEMgNDEuNjI1IDYyLjU0NyA0MC40MzEgNjEuMTM1IDM5Lj"
               "ExNiA1OS44NTIgQyAzNi4wNDkgNTYuODYxIDMyLjI0IDU1LjQyMSAyOC4wNDMgNTUuMTc0IEMgMjUuMzU0IDU1LjAxNiAyMi42Mjgg"
               "NTUuMTM0IDE5LjkzOSA1NS4zNzEgQyAxMi44ODUgNTUuOTkzIDYuNTg5IDUxLjc3MiA0Ljc0OSA0NS4wNDIgQyAyLjM1MSAzNi4yNz"
               "MgOS4wMjQgMjcuNDE4IDE4LjExNSAyNy40MjYgQyAyNi41OTMgMjcuNDM0IDMyLjgxOCAzMS40MTcgMzcuMDQ5IDM4LjYyNiBDIDM5"
               "LjU0MyA0Mi44NzYgNDAuOTkgNDcuNTQ5IDQyLjIyNCA1Mi4yODggQyA0My40MTcgNTYuODcxIDQ0LjIzNyA2MS41MjQgNDQuNzg2ID"
               "Y2LjIyNiBDIDQ0Ljc5NiA2Ni4zMDYgNDQuNzg4IDY2LjM4OCA0NC43ODggNjYuNTkyIFogIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGZp"
               "bGw9InJnYigwLDk3LDE2NSkiLz48cGF0aCBkPSIgTSA0NS44NjMgNTEuMjMgQyA0NS4zODYgNDkuNjc2IDQ0Ljk0NiA0OC4xMSA0NC"
               "40MjcgNDYuNTcgQyA0My4wNzMgNDIuNTQ4IDQxLjQyOSAzOC42NTMgMzguOTI1IDM1LjE5NSBDIDM2LjA5MiAzMS4yODQgMzIuMjk1"
               "IDI4LjQ4OSAyOC4xMzYgMjYuMTM3IEMgMjUuMDYyIDI0LjQgMjMuMDE4IDIxLjgzMSAyMi4zMTQgMTguMzU4IEMgMjEuMzY4IDEzLj"
               "Y4NiAyMi43NDggOS43MDUgMjYuNDc1IDYuNzQyIEMgMzAuMjU2IDMuNzM3IDM0LjUzNCAzLjM0MSAzOC44NjIgNS4zODEgQyA0My41"
               "MSA3LjU3MiA0Ni4wODggMTEuMjYyIDQ2LjA2NSAxNi41NTUgQyA0Ni4wMTUgMjcuOTEzIDQ1Ljk3NCAzOS4yNyA0NS45MzEgNTAuNj"
               "I4IEMgNDUuOTI5IDUwLjgyNyA0NS45MzEgNTEuMDI2IDQ1LjkzMSA1MS4yMjUgQyA0NS45MDggNTEuMjI3IDQ1Ljg4NSA1MS4yMjgg"
               "NDUuODYzIDUxLjIzIEwgNDUuODYzIDUxLjIzIFogIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9InJnYigwLDk3LDE2NSkiLz48cG"
               "F0aCBkPSIgTSA1Ny40MSA0LjE5MiBDIDYyLjAwNiA0LjE5NyA2NS43MzQgNy45MzQgNjUuNzMgMTIuNTM0IEMgNjUuNzI2IDE3LjEz"
               "IDYxLjk5IDIwLjg1NyA1Ny4zODkgMjAuODU0IEMgNTIuNzk0IDIwLjg1MSA0OS4wNjQgMTcuMTEyIDQ5LjA2OSAxMi41MTMgQyA0OS"
               "4wNzMgNy45MTYgNTIuODExIDQuMTg4IDU3LjQxIDQuMTkyIFogIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9InJnYigwLDk3LDE2"
               "NSkiLz48L2c+PC9nPjwvZz48L3N2Zz4="
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi






